
public class PayrollManagement {
	
	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		
		employees[0] = new SalariedEmployee(25000);
		employees[1] = new HourlyEmployee(40,10);
		employees[2] = new UnionizedHourlyEmployee(40,10,2000);
		employees[3] = new HourlyEmployee(40,10);
		employees[4] = new SalariedEmployee(25000);
		
		
		System.out.println(getTotalExpenses(employees));
		
	}
	
	
	public static double getTotalExpenses(Employee[] employees) {
		double totalExpense = 0;
		for(Employee e: employees) {
			totalExpense += e.getYearlyPay();
		}
		return totalExpense;
	}
}
