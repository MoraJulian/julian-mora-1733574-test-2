
public interface Employee {

		double getYearlyPay();
		
}
