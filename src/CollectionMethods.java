import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	
	
	
	
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){

		ArrayList<Planet> planetList = new ArrayList<Planet>();
		
		for(Planet p: planets) {
			if(p.getOrder() == 1 ||p.getOrder() == 2 || p.getOrder() == 3 ) {
				planetList.add(p);
			}
		}
		
		return planetList;
		
	}
	
}
