
public class UnionizedHourlyEmployee extends HourlyEmployee{

		private double pensionContribution;
		
		public UnionizedHourlyEmployee(double hoursWorked,double hourlyRate,double pensionContribution) {
			super(hoursWorked,hourlyRate);
			this.pensionContribution = pensionContribution;
		}
		
		public double getYearlyPay() {
			return super.getYearlyPay()+this.pensionContribution;
		}
		
	
}
