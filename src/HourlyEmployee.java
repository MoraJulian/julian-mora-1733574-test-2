
public class HourlyEmployee implements Employee{

	private double hoursWorked;
	private double hourlyRate;
	
	public HourlyEmployee(double hoursWorked,double hourlyRate) {
		this.hourlyRate = hourlyRate;
		this.hoursWorked = hoursWorked;
	}
	
	public double getYearlyPay() {
		return this.hoursWorked*this.hourlyRate*52;
	}
		
}
